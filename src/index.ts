import log from "@ajar/marker";
import fsp from "fs/promises";
import fs from "fs";

const jsonPath: string = "./dist/todos.json";

type Task = {
  description: string;
  isCompleted: boolean;
};

enum Command {
  Read = "--all",
  Update = "--update",
  Delete = "--delete",
  Create = "--create",
  Help = "--help",
}

function help() {
  log.yellow("These are the commands of the list :");
  log.info(
    "Create a new task - ",
    "npm run start -- list --create *taskDescription*"
  );
  log.info(
    "Update a task from completed to uncompleted (or opposite) - ",
    "npm run start -- list --update *taskID*"
  );
  log.info("Delete a task - ", "npm run start -- list --delete *taskID*");
  log.info("Show all tasks - ", "npm run start -- list --all");
  log.info("Show all of the commands of list - npm run start -- list --help");
}

async function handleCommand(argsInput: string[]) {
  if (
    argsInput[0] !== "list" ||
    !Object.values(Command).includes(argsInput[1] as Command)
  ) {
    log.red("invalid Input.");
    help();
  } else {
    switch (argsInput[1]) {
      case Command.Create:
        await createNewTask(argsInput[2]);
        break;
      case Command.Read:
        await showAllTasks();
        break;
      case Command.Update:
        await udpateTask(argsInput[2]);
        break;
      case Command.Delete:
        await deleteTask(argsInput[2]);
        break;
      case Command.Help:
        help();
        break;
    }
  }
}

async function deleteTask(taskID: string) {
  const toDoList: { [key: string]: Task } = await getToDoList();
  delete toDoList[taskID];
  await fsp.writeFile(jsonPath, JSON.stringify(toDoList, null, 2));
}

async function udpateTask(taskID: string) {
  const toDoList: { [key: string]: Task } = await getToDoList();
  toDoList[taskID].isCompleted = !toDoList[taskID].isCompleted;
  await fsp.writeFile(jsonPath, JSON.stringify(toDoList, null, 2));
}

async function showAllTasks() {
  const toDoList: { [key: string]: Task } = await getToDoList();
  for (const taskID in toDoList) {
    log.cyan(
      `${taskID}: `,
      `${toDoList[taskID].description} - ${
        toDoList[taskID].isCompleted ? "completed" : "uncompleted"
      }`
    );
  }
  await fsp.writeFile(jsonPath, JSON.stringify(toDoList, null, 2));
}

async function createNewTask(taskDescription: string) {
  const toDoList: { [key: string]: Task } = await getToDoList();
  const newTask: Task = { description: taskDescription, isCompleted: false };
  const uniqueID: string = "_" + Math.random().toString(16).slice(2);

  toDoList[uniqueID] = newTask;
  await fsp.writeFile(jsonPath, JSON.stringify(toDoList, null, 2));
}

async function getToDoList() {
  const fileExists = async (path: string) =>
    !!(await fs.promises.stat(path).catch((e) => false));

  if (await fileExists(jsonPath)) {
    return JSON.parse(await fsp.readFile(jsonPath, "utf-8"));
  } else {
    return {};
  }
}

async function init() {
  try {
    handleCommand(process.argv.slice(2));
  } catch (error) {}
}

await init();
